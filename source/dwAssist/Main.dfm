object MainForm: TMainForm
  Left = 400
  Top = 250
  Caption = 'dwAssist'
  ClientHeight = 461
  ClientWidth = 898
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Courier New'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  StyleName = 
    '[{"class":"TBitBtn","items":[{"name":"type","type":"string","hin' +
    't":"'#25353#38062#30340#26174#31034#26679#24335'","items":[["primary","'#20027#35201'"],["success","'#25104#21151'"],["info",' +
    '"'#20449#24687'"],["warning","'#35686#21578'"],["danger","'#21361#38505'"],["text","'#25991#26412'"]]},{"name":"' +
    'style","type":"'#25353#38062#30340#26174#31034#24418#29366'","items":[["plain","'#26420#32032'"],["round","'#22278#35282'"],[' +
    '"circle","'#22278#24418'"]]},{"name":"icon","type":"string","hint":"'#25353#38062#30340#22270#26631#65288#24038#20391 +
    #65289'","items":[["el-icon-platform-eleme"],["el-icon-eleme"],["el-ic' +
    'on-delete-solid"],["el-icon-delete"],["el-icon-s-tools"],["el-ic' +
    'on-setting"],["el-icon-user-solid"],["el-icon-user"],["el-icon-p' +
    'hone"],["el-icon-phone-outline"]]},{"name":"backgroundcolor","ty' +
    'pe":"string","hint":"'#25353#38062#30340#32972#26223#33394'","items":[["#4CAF50"],["#00AC6A"],["' +
    '#6CCF70"],["#008CBA"],["#0094D9"],["#00B4F4"],["#f44336"],["#FE5' +
    'E08"],["#FF6356"],["#e7e7e7"],["#D71103"],["#156AA8"],["#555555"' +
    ']]},{"name":"radius","type":"string","hint":"'#25353#38062#30340#22278#35282#21322#24452#65288#25968#20540#21487#33258#34892#26356#25913#65289'","' +
    'items":[["5px","5'#20687#32032#22278#35282'"],["50%","50%'#22278#35282'"],["5px 5px 0px 0px","'#24038#19978'/'#21491 +
    #19978#35282'5'#20687#32032#22278#35282#65292#24038#19979'/'#21491#19979#30452#35282'"],["10px 0 0 10px","'#24038#19978'/'#24038#19979#35282'5'#20687#32032#22278#35282#65292#21491#19978'/'#21491#19979#30452#35282'"],["0 10' +
    'px 10px 0","'#24038#19978'/'#24038#19979#35282#30452#35282#65292#21491#19978'/'#21491#19979'5'#20687#32032#22278#35282'"]]},{"name":"accept","type":"str' +
    'ing","hint":"'#25351#23450#19978#20256#25991#20214#30340#31867#22411'","items":[["image/*","'#19978#20256#22270#29255'"],["video/*","' +
    #19978#20256#35270#39057'"],["image/gif, image/jpeg","'#19978#20256'gif'#21644'jpeg"],[".doc,.docx,.xls,' +
    '.xlsx,.pdf","'#19978#20256#25351#23450#25193#23637#21517'"],["application/msword","WORD'#25991#26723'"],["applica' +
    'tion/pdf","'#19978#20256'PDF"]]}]},{"class":"TButton","items":[{"name":"type' +
    '","type":"string","hint":"'#25353#38062#30340#26174#31034#26679#24335'","items":[["primary","'#20027#35201'"],["s' +
    'uccess","'#25104#21151'"],["info","'#20449#24687'"],["warning","'#35686#21578'"],["danger","'#21361#38505'"],["t' +
    'ext","'#25991#26412'"]]},{"name":"style","type":"string","hint":"'#25353#38062#30340#26174#31034#24418#29366'","i' +
    'tems":[["plain","'#26420#32032'"],["round","'#22278#35282'"],["circle","'#22278#24418'"]]},{"name":"' +
    'icon","type":"string","hint":"'#25353#38062#30340#22270#26631#65288#24038#20391#65289'","items":[["el-icon-plat' +
    'form-eleme"],["el-icon-eleme"],["el-icon-delete-solid"],["el-ico' +
    'n-delete"],["el-icon-s-tools"],["el-icon-setting"],["el-icon-use' +
    'r-solid"],["el-icon-user"],["el-icon-phone"],["el-icon-phone-out' +
    'line"]]},{"name":"backgroundcolor","type":"string","hint":"'#25353#38062#30340#32972#26223 +
    #33394'","items":[["#4CAF50"],["#00AC6A"],["#6CCF70"],["#008CBA"],["#0' +
    '094D9"],["#00B4F4"],["#f44336"],["#FE5E08"],["#FF6356"],["#e7e7e' +
    '7"],["#D71103"],["#156AA8"],["#555555"]]},{"name":"radius","type' +
    '":"string","hint":"'#25353#38062#30340#22278#35282#21322#24452#65288#25968#20540#21487#33258#34892#26356#25913#65289'","items":[["5px","5'#20687#32032#22278#35282'"],["' +
    '50%","50%'#22278#35282'"],["5px 5px 0px 0px","'#24038#19978'/'#21491#19978#35282'5'#20687#32032#22278#35282#65292#24038#19979'/'#21491#19979#30452#35282'"],["10px 0' +
    ' 0 10px","'#24038#19978'/'#24038#19979#35282'5'#20687#32032#22278#35282#65292#21491#19978'/'#21491#19979#30452#35282'"],["0 10px 10px 0","'#24038#19978'/'#24038#19979#35282#30452#35282#65292#21491#19978'/'#21491#19979 +
    '5'#20687#32032#22278#35282'"]]}]},{"class":"TColumnTitle","items":[{"name":"type","typ' +
    'e":"'#23383#27573#31867#22411'","items":[["image","'#26174#31034#22270#20687'"],["rate","'#26174#31034#20026#24509#31456'"]]},{"name":"' +
    'left","type":"integer","hint":"left","items":[[10],[20],[30],[40' +
    '],[50],[60],[80],[100],[150],[200]]},{"name":"top","type":"integ' +
    'er","hint":"top","items":[[10],[20],[30],[40],[50],[60],[80],[10' +
    '0]]},{"name":"width","type":"integer","hint":"width","items":[[4' +
    '0],[60],[80],[100],[120],[150],[180],[200]]},{"name":"height","t' +
    'ype":"integer","hint":"height","items":[[40],[60],[80],[100],[12' +
    '0],[150],[180],[200]]},{"name":"right","type":"integer","hint":"' +
    'right","items":[[10],[20],[30],[40],[50],[60],[80],[100],[150],[' +
    '200]]},{"name":"bottom","type":"integer","hint":"top","items":[[' +
    '10],[20],[30],[40],[50],[60],[80],[100]]},{"name":"nowidth","typ' +
    'e":"integer","hint":"not use width","items":[[1]]},{"name":"nohe' +
    'ight","type":"integer","hint":"not use height","items":[[1]]},{"' +
    'name":"format","type":"string","hint":"'#29992#20110#26684#24335#21270#23383#27573#20540'","items":[["medi' +
    'a/images/%s.png"]]}]},{"class":"TComboBox","items":[{"name":"hei' +
    'ght","type":"integer","hint":"'#29992#20110#35299#20915'TComboBox'#30340#39640#24230#38382#39064'","items":[["25"' +
    '],["30"],["35"],["40"]]}]},{"class":"TEdit","items":[{"name":"pr' +
    'efix-icon","type":"string","hint":"'#25353#38062#30340#22270#26631#65288#24038#20391#65289'","items":[["el-icon' +
    '-platform-eleme"],["el-icon-eleme"],["el-icon-delete-solid"],["e' +
    'l-icon-delete"],["el-icon-s-tools"],["el-icon-setting"],["el-ico' +
    'n-user-solid"],["el-icon-user"],["el-icon-phone"],["el-icon-phon' +
    'e-outline"]]},{"name":"suffix-icon","type":"string","hint":"'#25353#38062#30340#22270 +
    #26631#65288#21491#20391#65289'","items":[["el-icon-platform-eleme"],["el-icon-eleme"],["e' +
    'l-icon-delete-solid"],["el-icon-delete"],["el-icon-s-tools"],["e' +
    'l-icon-setting"],["el-icon-user-solid"],["el-icon-user"],["el-ic' +
    'on-phone"],["el-icon-phone-outline"]]},{"name":"placeholder","ty' +
    'pe":"string","hint":"'#36755#20837#25552#31034'","items":[["'#35831#36755#20837#29992#25143#21517'"],["'#35831#36755#20837#23494#30721'"],["'#25628#32034'"],' +
    '["search"]]},{"name":"dwattr","type":"string","hint":"'#30452#25509'HTML'#33410#28857#23646#24615 +
    '","items":[["type=\"number\""],["type=\"color\""],["type=\"date\' +
    '""],["type=\"email\""],["type=\"week\""],["type=\"time\""],["typ' +
    'e=\"datetime-local\""]]}]},{"class":"TForm","items":[{"name":"ta' +
    'blename","type":"string","hint":"'#29992#20110'CRUD'#26102#30340#34920#21517'","items":[["user"],[' +
    '"product"],["role"]]},{"name":"pagesize","type":"integer","hint"' +
    ':"'#27599#39029#26174#31034#35760#24405#25968#37327'","items":[[5],[10],[15],[20],[30],[50]]},{"name":"row' +
    'height","type":"integer","hint":"'#34892#39640'","items":[[25],[30],[35],[40' +
    '],[45],[50]]},{"name":"fields","type":"string","hint":"'#23383#27573#21015#34920'","it' +
    'ems":[["name"]]}]},{"class":"TImage","items":[{"name":"src","typ' +
    'e":"string","hint":"'#23545#24212#30340#22270#29255#25991#20214#22320#22336'","items":[["media/images/mn/1.jpg"' +
    '],["image/1.jpg"]]},{"name":"radius","type":"string","hint":"'#22278#35282#21322 +
    #24452#65288#25968#20540#21487#33258#34892#26356#25913#65289'","items":[["5px","5'#20687#32032#22278#35282'"],["50%","50%'#22278#35282'"],["5px 5px 0' +
    'px 0px","'#24038#19978'/'#21491#19978#35282'5'#20687#32032#22278#35282#65292#24038#19979'/'#21491#19979#30452#35282'"],["10px 0 0 10px","'#24038#19978'/'#24038#19979#35282'5'#20687#32032#22278#35282#65292#21491#19978'/' +
    #21491#19979#30452#35282'"],["0 10px 10px 0","'#24038#19978'/'#24038#19979#35282#30452#35282#65292#21491#19978'/'#21491#19979'5'#20687#32032#22278#35282'"]]},{"name":"href",' +
    '"type":"string","hint":"'#28857#20987#36339#36716#32593#22336#65288#22312#26032#39029#38754#25171#24320#65289'","items":[["/hello"]]},{"' +
    'name":"hrefself","type":"string","hint":"'#28857#20987#36339#36716#32593#22336#65288#22312#24403#21069#39029#38754#25171#24320#65289'","items' +
    '":[["/hello"]]}]},{"class":"TLabel","items":[]},{"class":"TListV' +
    'iew","items":[{"name":"dataset","type":"'#23545#24212#30340'FDQuery'#25511#20214#65292#22914'FDQuery1",' +
    '"items":[["FDQuery1"],["FDQuery2"],["FDQuery"]]},{"name":"primar' +
    'ykey","type":"DataSet'#30340#20027#38190#65292#27604#22914'id","items":[["id"],["uid"]]},{"name"' +
    ':"topheight","type":"integer","hint":"'#39030#37096#26639#39640#24230'","items":[[30],[40],' +
    '[50],[60],[70],[80],[90],[100]]},{"name":"rowheight","type":"int' +
    'eger","hint":"'#35760#24405#34892#39640#24230'","items":[[30],[35],[40],[45],[50],[60]]},{"' +
    'name":"pagesize","type":"integer","hint":"'#27599#39029#35760#24405#25968'","items":[[10],[' +
    '20],[30],[40],[50],[60],[70],[80],[90],[100]]},{"name":"db","typ' +
    'e":"'#25968#25454#24211#31867#22411'","items":[["access"],["mssql2000"],["mssql2008"],["mss' +
    'ql2014"],["mysql"],["sqlite"]]}]},{"class":"TPanel","items":[{"n' +
    'ame":"type","type":"string","hint":"'#24509#31456#26679#24335#65288'HelpKeyword'#20026'badge'#26102#26377#25928#65289'",' +
    '"items":[["primary"],["success"],["info"],["warning"],["danger"]' +
    ',["text"]]},{"name":"radius","type":"string","hint":"'#25353#38062#30340#22278#35282#21322#24452#65288#25968#20540#21487 +
    #33258#34892#26356#25913#65289'","items":[["5px","5'#20687#32032#22278#35282'"],["50%","50%'#22278#35282'"],["5px 5px 0px 0p' +
    'x","'#24038#19978'/'#21491#19978#35282'5'#20687#32032#22278#35282#65292#24038#19979'/'#21491#19979#30452#35282'"],["10px 0 0 10px","'#24038#19978'/'#24038#19979#35282'5'#20687#32032#22278#35282#65292#21491#19978'/'#21491#19979#30452#35282'"' +
    '],["0 10px 10px 0","'#24038#19978'/'#24038#19979#35282#30452#35282#65292#21491#19978'/'#21491#19979'5'#20687#32032#22278#35282'"]]},{"name":"dwstyle","t' +
    'ype":"string","hint":"'#30452#25509#35774#32622'HTML'#20803#32032'style","items":[["background-ima' +
    'ge: linear-gradient(#7A88FF, #7AFFAF);","'#20174#19978#21040#19979#28176#21464'"],["background-i' +
    'mage: linear-gradient(to right , #7A88FF, #7AFFAF);","'#20174#24038#21040#21491#28176#21464'"]]}' +
    ']}]'
  PixelsPerInch = 96
  TextHeight = 16
  object Panel_Buttons: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 418
    Width = 892
    Height = 40
    Hint = '{"type":"primary","radius":"5px 5px 0px 10px"}'
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Shape_JSON: TShape
      AlignWithMargins = True
      Left = 10
      Top = 12
      Width = 20
      Height = 16
      Margins.Left = 10
      Margins.Top = 12
      Margins.Bottom = 12
      Align = alLeft
      Brush.Color = clLime
      Shape = stCircle
      ExplicitTop = 10
      ExplicitHeight = 20
    end
    object Label_JSON: TLabel
      Left = 33
      Top = 0
      Width = 94
      Height = 40
      Align = alLeft
      AutoSize = False
      Caption = 'not JSON'
      Layout = tlCenter
      ExplicitLeft = 36
      ExplicitTop = 3
    end
    object Button_SaveJson: TButton
      AlignWithMargins = True
      Left = 645
      Top = 5
      Width = 75
      Height = 30
      Margins.Top = 5
      Margins.Bottom = 5
      Align = alRight
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
      OnClick = Button_SaveJsonClick
    end
    object Button_Cancel: TButton
      AlignWithMargins = True
      Left = 807
      Top = 5
      Width = 75
      Height = 30
      Margins.Top = 5
      Margins.Right = 10
      Margins.Bottom = 5
      Align = alRight
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object Button_SaveText: TButton
      AlignWithMargins = True
      Left = 726
      Top = 5
      Width = 75
      Height = 30
      Margins.Top = 5
      Margins.Bottom = 5
      Align = alRight
      Caption = 'Save'
      ModalResult = 1
      TabOrder = 2
    end
    object Button_Format: TButton
      AlignWithMargins = True
      Left = 130
      Top = 5
      Width = 75
      Height = 30
      Hint = 
        '{"type":"danger","backgroundcolor":"#FE5E08","radius":"5px","sty' +
        'le":"plain","icon":"el-icon-delete-solid"}'
      Margins.Top = 5
      Margins.Bottom = 5
      Align = alLeft
      Caption = 'Format'
      TabOrder = 3
      OnClick = Button_FormatClick
    end
  end
  object Panel_Client: TPanel
    Left = 0
    Top = 0
    Width = 898
    Height = 415
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel_Client'
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 513
      Top = 0
      Width = 5
      Height = 415
      ExplicitLeft = 220
      ExplicitHeight = 318
    end
    object PageControl1: TPageControl
      Left = 518
      Top = 0
      Width = 380
      Height = 415
      Margins.Left = 0
      Align = alClient
      MultiLine = True
      TabOrder = 0
    end
    object SynEdit: TSynEdit
      Left = 0
      Top = 0
      Width = 513
      Height = 415
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Consolas'
      Font.Style = []
      Font.Quality = fqClearTypeNatural
      TabOrder = 1
      UseCodeFolding = False
      Gutter.Font.Charset = DEFAULT_CHARSET
      Gutter.Font.Color = clWindowText
      Gutter.Font.Height = -11
      Gutter.Font.Name = 'Consolas'
      Gutter.Font.Style = []
      Gutter.Visible = False
      Gutter.Width = 0
      Highlighter = SynJSONSyn
      Options = [eoAutoIndent, eoDragDropEditing, eoEnhanceEndKey, eoGroupUndo, eoScrollPastEol, eoShowScrollHint, eoSmartTabDelete, eoTabIndent, eoTabsToSpaces]
      TabWidth = 4
      WantTabs = True
      OnChange = Memo_ValueChange
    end
  end
  object SynJSONSyn: TSynJSONSyn
    Options.AutoDetectEnabled = False
    Options.AutoDetectLineLimit = 0
    Options.Visible = False
    Left = 256
    Top = 160
  end
end
